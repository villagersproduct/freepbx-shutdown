echo "" >> /etc/asterisk/extensions_custom.conf
echo "include freepbx-shutdown-reboot-dialplan.conf" >> /etc/asterisk/extensions_custom.conf
cp freepbx-shutdown-reboot-dialplan.conf /etc/asterisk
chown asterisk:asterisk freepbx-shutdown-reboot-dialplan.conf
chmod 777 freepbx-shutdown-reboot-dialplan.conf
sudo adduser asterisk sudo
echo "asterisk ALL=(ALL:ALL) ALL" >> /etc/sudoers
echo "%sudo ALL= NOPASSWD: /sbin/shutdown" >> /etc/sudoers
